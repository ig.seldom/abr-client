const { ipcRenderer } = require('electron')
const axios = require('axios')

let title = ""
let track = ""

let checkNowPlayingInfo = () => {
    let newTitle = document.querySelector(".now-playing-block.right .info .title").innerText
    let newTrack = document.querySelector(".now-playing-block.right .info .track").innerText
    if(title !== newTitle && track !== newTrack) {
        title = newTitle
        track = newTrack
        ipcRenderer.invoke('now-playing-changed', title, track)
    }
}

setInterval(checkNowPlayingInfo, 1000)