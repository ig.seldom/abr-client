const { app, BrowserWindow, ipcMain, Notification, nativeImage } = require('electron')
const path = require("node:path")
let mainWindow = null

const createWindow = () => {
    mainWindow = new BrowserWindow({
        width: 800,
        height: 600,
        webPreferences: {
            nodeIntegration: false,
            preload: path.join(__dirname, 'preload.js'),
        },
        icon: path.join(__dirname, "img", "logo.png")
    })
    mainWindow.loadURL("https://planetradio.co.uk/absolute-radio/player/")
}

const showNotification = (title, body) => {
    let n = new Notification({ title: title, body: body, silent: true})
    n.show()
}

ipcMain.handle('now-playing-changed', async (event, title, track) => {
    showNotification(title, track)
})

app.whenReady().then(createWindow)

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on("activate", () => {
    if(mainWindow === null) {
        createWindow()
    }
})